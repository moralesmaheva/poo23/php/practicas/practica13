<?php
//crear funcion que aceptas dos numeros como argumentos y te imprime la suma

/**
 * Calcula la suma de dos números y muestra el resultado.
 *
 * @param int $numero1 El primer número.
 * @param int $numero2 El segundo número.
 * @return void
 */
function sumaDos($numero1, $numero2)
{
    $suma = $numero1 + $numero2;
    echo $suma;
}


//funcion que acepta tres numeros como argumentos y te imprime la suma

/**
 * Calcula la suma de tres números.
 *
 * @param int $numero1 El primer número.
 * @param int $numero2 El segundo número.
 * @param int $numero3 El tercer número.
 * @return void
 */
function sumaTres($numero1, $numero2, $numero3)
{
    $suma = $numero1 + $numero2 + $numero3;
    echo $suma;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion sumaDos() -->
        <?= sumaDos(5, 10) ?>
    </div>
    <div>
        <!-- llamada a la funcion sumaTres() -->
        <?= sumaTres(5, 10, 15) ?>
    </div>
</body>

</html>