<?php
//crear array que le pasas n numeros como array y te devuelve la suma de todos ellos

/**
 * Calcula la suma de todos los elementos en un arreglo.
 *
 * @param array $numeros Un arreglo de números.
 * @return int La suma de todos los elementos en el arreglo.
 */
function sumArray(array $numeros)
{
    $salida = array_sum($numeros);
    return $salida;
}

//inicializar el array
$numeros = [4, 7, 9, 30];

//llamada a la funcion
$salida = sumArray($numeros);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 14</title>
</head>

<body>
    <div>
        <!-- impresion salida -->
        <?= $salida ?>
    </div>
</body>

</html>