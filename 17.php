<?php
//crear funcion que le pasas un string como argumento y de devuelve el 
//numero de vocales que tiene

//opcion 1 con for

function vocales(string $texto)
{
    $vocales = 0;
    for ($c = 0; $c < strlen($texto); $c++) {
        if (in_array($texto[$c], ["a", "e", "i", "o", "u"])) {
            $vocales++;
        }
    }
    return $vocales;
}

//opcion 2 con substr_count

function vocales2(string $texto)
{
    $a = substr_count($texto, "a");
    $e = substr_count($texto, "e");
    $i = substr_count($texto, "i");
    $o = substr_count($texto, "o");
    $u = substr_count($texto, "u");
    return $a + $e + $i + $o + $u;
}

//opcion 3 con switch

function vocales3(string $texto)
{
    $vocales = 0;
    for ($c = 0; $c < strlen($texto); $c++) {
        switch ($texto[$c]) {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
                $vocales++;
                break;
        }
    }
    return $vocales;
}

//inicializar el string
$texto = "aabbffcceeoou";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 17</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion vocales -->
        <h1>Con for</h1>
        <?= vocales($texto) ?>
    </div>
    <div>
        <!-- llamada a la funcion vocales2 -->
        <h1>Con substr_count</h1>
        <?= vocales2($texto) ?>
    </div>
    <div>
        <!-- llamada a la funcion vocales3 -->
        <h1>Con switch</h1>
        <?= vocales3($texto) ?>
    </div>
</body>

</html>