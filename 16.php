<?php
//crear una variable global llamada resultado 
//crear funcion que le pasas dos numeros y te almacena el resultado en la variable global


/**
 * Una función que toma dos números como parámetros y devuelve su suma.
 *
 * @param mixed $numero1 El primer número.
 * @param mixed $numero2 El segundo número.
 * @return mixed La suma de los dos números.
 */
function dosNumeros($numero1, $numero2)
{
    global $resultado;
    $resultado = $numero1 + $numero2;
    return $resultado;
}

//llamada a la funcion
dosNumeros(2, 3);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 16</title>
</head>

<body>
    <div>
        <!-- impresion resultado -->
        <?= $resultado ?>
    </div>
</body>

</html>