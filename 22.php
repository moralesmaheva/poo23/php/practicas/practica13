<?php
//crear una funcion que le pasas dos arrays del mismo tamaño 
//y me devuelve el numero de coincidencias en estos dos arrays

function coincidencias(array $array1, array $array2)
{
    $contador = 0;
    for ($c = 0; $c < count($array1); $c++) {
        if ($array1[$c] == $array2[$c]) {
            $contador++;
        }
    }
    return $contador;
}

//inicializamos dos arrays
$datos1 = [3, 5, 6, 2, 8, 2, 9];
$datos2 = [3, 3, 7, 2, 8, 1, 9];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 22</title>
</head>

<body>
    <!-- llamada a la funcion e impresion -->
    <div>
        <?= coincidencias($datos1, $datos2) ?>
    </div>
</body>

</html>