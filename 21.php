<?php
//funcion que recibe como argumento un array y una operacion a realizar con su elementos
//pueden ser suma, producto o resta

function calculadora(array $array, string $operacion)
{
    if ($operacion == "+") {
        return array_sum($array);
    } elseif ($operacion == "*") {
        return array_product($array);
    } elseif ($operacion == "-") {
        $resta = 0;
        for ($c = 0; $c < count($array); $c++) {
            $resta = $resta - $array[$c];
        }
        return $resta;
    }else{
        return "operacion no valida";
    }
}

//inicializamos un array
$datos = [3, 5, 6, 2, 8, 1, 9];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 21</title>
</head>
<body>
    <!-- llamada a la funcion e impresion -->
    <div>
        <?= calculadora($datos, "+") ?>
    </div>
    <div>
        <?= calculadora($datos, "*") ?>
    </div>
    <div>
        <?= calculadora($datos, "-") ?>
    </div>
    <div>
        <?= calculadora($datos, "x") ?>
    </div>
</body>
</html>