<?php
//crear funcion que le pasas un array con tres colores y te imprime 3 divs de 
// 100px de alto cada uno con el color correspondiente de array pasado como argumento

//opcion 1 con for

/**
 * Genera una serie de divs de colores.
 *
 * @param array $colores Un array que contiene tres valores de color que representan 
 * el color de fondo de cada div.
 */
function colores(array $colores)
{
    for ($c = 0; $c < 3; $c++) {
        echo "<div style='background-color: {$colores[$c]}; height: 100px;'></div>";
    }
}

//opcion 2 con foreach

/**
 * Imprime una serie de elementos div con diferentes colores de fondo.
 *
 * @param array $colores Un array de colores.
 */
function colores2(array $colores)
{
    foreach ($colores as $color) {
        echo "<div style='background-color: {$color}; height: 100px;'></div>";
    }
}

//inicializar el array
$colores = ["yellow","purple","green"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 13</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion colores -->
        <h1>Con for</h1>
        <?= colores($colores) ?>
    </div>
    <div>
        <!-- llamada a la funcion colores2 -->
        <h1>Con foreach</h1>
        <?= colores2($colores) ?>
    </div>
</body>

</html>