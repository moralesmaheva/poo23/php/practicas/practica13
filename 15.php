<?php
//crea una funcion que le pasas n numeros como un array y te devuelve
//el producto de todos ellos

/**
 * Calcula el producto de todos los números en un arreglo.
 *
 * @param array $numeros Un arreglo de números.
 * @return mixed El producto de todos los números en el arreglo.
 */
function productoArray(array $numeros)
{
    $salida = array_product($numeros);
    return $salida;
}

//inicializar el array
$numeros = [3, 5, 2, 1];

//llamada a la funcion
$salida = productoArray($numeros);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 15</title>
</head>

<body>
    <div>
        <!-- impresion salida -->
        <?= $salida ?>
    </div>
</body>

</html>