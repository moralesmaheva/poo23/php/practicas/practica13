<?php
//crear funcion que le pasas un array y te lo imprime al reves

//opcion 1 con for

/**
 * Imprime los números en orden inverso.
 *
 * @param array $numeros El array de números a imprimir.
 * @return void
 */
function imprimeReves(array $numeros)
{
    for ($c = count($numeros) - 1; $c >= 0; $c--) {
        $resultado = $numeros[$c];
        echo $resultado . "<br>";
    }
}

//opcion 2 con la funcion array_reverse y foreach

function imprimeReves2(array $numeros)
{
    $numeros = array_reverse($numeros);
    foreach ($numeros as $valor) {
        echo $valor . "<br>";
    }
}

//inicializar el array
$numeros = [4, 7, 9, 30];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 11</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion imprimeReves -->
        <h1>Con for</h1>
        <?= imprimeReves($numeros) ?>
    </div>
    <div>
        <!-- llamada a la funcion imprimeReves2 -->
        <h1>Con foreach y la funcion array_reverse</h1>
        <?= imprimeReves2($numeros) ?>
    </div>
</body>

</html>