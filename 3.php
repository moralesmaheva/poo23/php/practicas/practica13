<?php
//crear funcion que imprima numeros entre dos argumentos pasados

/**
 * Genera una lista de números impares entre un número inicial y un número final dados.
 *
 * @param int $numinicio El número inicial del rango.
 * @param int $numfinal El número final del rango.
 * @return void
 */
function numerosImp($numinicio, $numfinal)
{
    for ($c = $numinicio; $c <= $numfinal; $c++) {
        echo $c . "<br>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?= numerosImp(5, 15) ?>
    </div>
</body>

</html>