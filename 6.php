<?php
//crear una funcion que le pasas un array de numeros y los imprime

/**
 * Itera sobre un array y muestra cada valor con un salto de línea.
 *
 * @param array $numeros El array de números sobre el cual iterar.
 * @return void
 */
function impArray(array $numeros)
{
    foreach ($numeros as $valor) {
        echo $valor . "<br>";
    }
}

//iniciar el array
$numeros = [3, 6, 9, 12, 15, 18, 21, 24, 27, 30];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?= impArray($numeros) ?>
    </div>
</body>

</html>