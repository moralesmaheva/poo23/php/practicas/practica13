<?php
//crear funcion que imprima desde el 1 al 100

/**
 * Genera números del 0 al 100 y los imprime.
 *
 * Esta función genera números del 0 al 100 utilizando un bucle for y los imprime, 
 * seguidos de un salto de línea.
 */
function cienNumeros()
{
    for ($c = 0; $c <= 100; $c++) {
        echo $c . "<br>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?= cienNumeros() ?>
    </div>
</body>

</html>