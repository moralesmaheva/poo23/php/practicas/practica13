<?php
//funcion que recibe como argumento dos numeros y una operacion a realizar con ellos que
//puede ser suma, producto o resta
//la funcion devuelve el resultado de la operacion

function calculadora(int $num1, int $num2, string $operacion)
{
    switch ($operacion) {
        case "+":
            return $num1 + $num2;
        case "*":
            return $num1 * $num2;
        case "-":
            return $num1 - $num2;
        default:
            return "operacion no valida";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 20</title>
</head>

<body>
    <!-- llamada a la funcion e impresion -->
    <div>
        <?= calculadora(2, 3, "+") ?>
    </div>
    <div>
        <?= calculadora(2, 3, "*") ?>
    </div>
    <div>
        <?= calculadora(2, 3, "-") ?>
    </div>
    <div>
        <?= calculadora(2, 3, "x") ?>
    </div>
</body>

</html>