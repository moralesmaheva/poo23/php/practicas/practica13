<?php
//crear una funcion que imprima 10 numeros aleatorios

/**
 * Genera y muestra 10 números aleatorios entre 1 y 100.
 *
 * @return void
 */
function numeros()
{
    for ($c = 0; $c < 10; $c++) {
        echo rand(1, 100) . "<br>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?= numeros() ?>
    </div>
</body>

</html>