<?php
//crear una funcion que le pasas un string como argumento y de devuelve 
//un arrray con las vocales que tiene

//opcion 1 con for y in_array

function vocalesArray(string $texto)
{
    $vocales = [];
    for ($c = 0; $c < strlen($texto); $c++) {
        if (in_array($texto[$c], ["a", "e", "i", "o", "u"])) {
            $vocales[] = $texto[$c];
        }
    }
    return $vocales;
}

//opcion 2 con for y elseif

function vocalesArray2(string $texto){
    for($c=0;$c<strlen($texto);$c++){
        if($texto[$c]=="a"){
            $vocales[] = $texto[$c];
        }elseif($texto[$c]=="e"){
            $vocales[] = $texto[$c];
        }elseif($texto[$c]=="i"){
            $vocales[] = $texto[$c];
        }elseif($texto[$c]=="o"){
            $vocales[] = $texto[$c];
        }elseif($texto[$c]=="u"){
            $vocales[] = $texto[$c];
        }
    }
    return $vocales;
}

//llamada a la funcion vocalesArray
var_dump(vocalesArray("aabbffcceeoou"));

//llamada a la funcion vocalesArray2
var_dump(vocalesArray2("aabbffcceeoou"));
