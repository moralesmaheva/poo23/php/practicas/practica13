<?php
//crear funcion que le pasas un array de numeros y comprueba si son todos positivos
//si son positivos devuelve Todos positivos y si hay algun negativo devuelve hay negativos

function positivos(array $numeros)
{
    foreach ($numeros as $valor) {
        if ($valor < 0) {
            return "Hay negativos";
        }
    }
    return "Todos positivos";
}

//iniciar el array
$numeros = [4, 7, 9, -12, 15, 18, 21, 24, -27, 30];
$numeros2 = [2, 4, 6, 78, 12];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 9</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?= positivos($numeros) ?>
    </div>
    <div>
        <!-- llamada a la funcion -->
        <?= positivos($numeros2) ?>
    </div>
</body>

</html>