<?php
//crear funcion que le pasas dos numeros y si son iguales te escribe iguales y si no distintos

/**
 * Comprueba si dos números son iguales y muestra el resultado.
 *
 * @param mixed $numero1 El primer número a comparar.
 * @param mixed $numero2 El segundo número a comparar.
 * @return void
 */
function iguales($numero1, $numero2)
{
    if ($numero1 == $numero2) {
        echo "Iguales";
    } else {
        echo "Distintos";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?php
        iguales(5, 7)
        ?>
    </div>
    <div>
        <!-- llamada a la funcion -->
        <?php
        iguales(5, 5)
        ?>
    </div>
</body>

</html>