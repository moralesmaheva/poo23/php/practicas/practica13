<?php
//crear una funcion que le pasas 3 numeros y te devuelve el producto

/**
 * Multiplica tres números y devuelve el producto.
 *
 * @param int $numero1 El primer número.
 * @param int $numero2 El segundo número.
 * @param int $numero3 El tercer número.
 * @return int El producto de los tres números.
 */
function producto(int $numero1, int $numero2, int $numero3): int
{
    $producto = $numero1 * $numero2 * $numero3;
    return $producto;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?= producto(5, 1, 3) ?>
    </div>
</body>

</html>