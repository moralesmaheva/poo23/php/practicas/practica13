<?php
//crear funcion que le pasas un array y te llo devuelve ordenado

function ordenar(array $array)
{
    sort($array);
    return $array;
}

//inicializamos un array
$datos = [3, 5, 6, 2, 8, 1, 9];
$salida = "";
$salidaOrdenada = "";
//utilizamos un for para imprimir los datos
for ($c = 0; $c < count($datos); $c++) {
    $salida = $salida . $datos[$c] . "<br>";
}

//llamamos a la funcion
$datosOrdenados = ordenar($datos);

//utilizamos un foreach para imprimir los datos ordenados
foreach ($datosOrdenados as $dato) {
    $salidaOrdenada = $salidaOrdenada . $dato . "<br>";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 19</title>
</head>

<body>
    <h2>Datos</h2>
    <div>
        <?= $salida ?>
    </div>
    <h2>Datos Ordenados</h2>
    <div>
        <?= $salidaOrdenada ?>
    </div>
</body>

</html>