<?php
//funcion que le pasas un texto y lo imprime al reves

//opcion 1 con funcion strrev

/**
 * Invierte el texto dado y muestra el resultado.
 *
 * @param string $texto El texto a invertir.
 * @return void
 */
function imprimeReves($texto)
{
    $salida = strrev($texto);
    echo $salida;
}

//opcion 2 con for

/**
 * Imprime el texto dado en orden inverso.
 *
 * @param string $texto El texto a imprimir.
 * @return void
 */
function imprimeReves2($texto)
{
    for ($c = strlen($texto) - 1; $c >= 0; $c--) {
        $salida = $texto[$c];
        echo $salida;
    }
}
//inicializamos la variable
$texto = "hola mundo";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 12</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion imprimeReves -->
        <h1>Con strrev</h1>
        <?= imprimeReves($texto) ?>
    </div>
    <div>
        <!-- llamada a la funcion imprimeReves2 -->
        <h1>Con for</h1>
        <?= imprimeReves2($texto) ?>
    </div>
</body>

</html>