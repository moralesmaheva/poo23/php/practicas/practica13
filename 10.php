<?php
//crear funcion que le pasas tres arrays y te imprime la suma de ellos
//devuelve un numero

/**
 * Suma los elementos de tres arreglos y devuelve la suma total.
 *
 * @param array $numeros1 Un arreglo de números.
 * @param array $numeros2 Un arreglo de números.
 * @param array $numeros3 Un arreglo de números.
 * @return int La suma de todos los números en los tres arreglos.
 */
function sumArray(array $numeros1, array $numeros2, array $numeros3)
{
    $suma1 = array_sum($numeros1);
    $suma2 = array_sum($numeros2);
    $suma3 = array_sum($numeros3);
    return $suma1 + $suma2 + $suma3;
}

//inicilizamos los arrays

$numeros1 = [1, 2, 3, 4, 5];
$numeros2 = [6, 7, 8, 9, 10];
$numeros3 = [11, 12, 13, 14, 15];

$resultado = sumArray($numeros1, $numeros2, $numeros3);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 10</title>
</head>

<body>
    <div>
        <!-- impresion resultado -->
        <?= $resultado ?>
    </div>
</body>

</html>