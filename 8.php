<?php
//funcion que le pasas dos numeros y te devuelve si son iguales o distintos

/**
 * Determina si dos números son iguales o no.
 *
 * @param mixed $numero1 El primer número.
 * @param mixed $numero2 El segundo número.
 * @return string El resultado de la comparación: "Iguales" si los números son iguales, 
 * "Distintos" en caso contrario.
 */
function iguales($numero1, $numero2)
{
    if ($numero1 == $numero2) {
        $salida = "Iguales";
    } else {
        $salida = "Distintos";
    }
    return $salida;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8</title>
</head>

<body>
    <div>
        <!-- llamada a la funcion -->
        <?php
        echo iguales(5, 7);
        ?>
    </div>
    <div>
        <!-- llamada a la funcion -->
        <?php
        echo iguales(5, 5);
        ?>
    </div>
</body>

</html>